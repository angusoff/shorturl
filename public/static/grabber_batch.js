/*|--------------------------------------------------------------------------------------
|*|
|*|--------------------------------------------------------------------------------------
|*| var c = "https://su.rpt01.com/static/grabber_batch.js" ;
|*|--------------------------------------------------------------------------------------*/
(function(){
    console.log("in https://su.rpt01.com/static/grabber_batch.js ") ;
	// alert('grabber_batch.js');

    if(document.getElementById("grabber-batch-flag")!=null){
		alert("已在批次中");
		return;
	}

	var is_picture = false;
    var script = document.getElementById("grabber-script");
    var script_url = script.src;
    // var css_url  = dirname(script_url) + '/grabber_batch.css';
    var css_url  =  'https://su.rpt01.com/static/grabber_batch.css';

    a=document.createElement("link"),a.rel='stylesheet',a.type='text/css',a.href=css_url,a.media='all',document.body.appendChild(a);
    (function(){
        (a=document.createElement("script")).src="//ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js";
        document.body.appendChild(a);
        a.onload = function(){ // 等待 jQuery 載入完成
            // 這裡可以放一些事前作業

			var selector_args = {}; // 選擇器參數
			switch(true){
				case location.href.indexOf('missav.com')>-1:
				case location.href.indexOf('www.missav.com')>-1:
					// alert("決定好 missav.com");
					break;
				case location.href.indexOf('mhn.quest')>-1:
					break;
				default:
					alert("無對應的模組");
					return;
				/* 說明區
				case location.href.indexOf('hanime1.me')>-1:
					selector_args = {
						"tag":".home-rows-videos-wrapper>a",
						"tag_val":"text",
						"main_a":"全選時jquery selector選擇器指向位置", // .home-rows-videos-wrapper>a div, .home-rows-videos-wrapper div.multiple-link-wrapper , .row div a
						"click_a":"需點選的影片位置",
						"click_parent":null
					};
					break;
				*/
			}
			grabber_init( selector_args);
        };
    })();



    function grabber_init( selector_args){
		console.log("grabber_init");

		var batch_type_name = is_picture?"圖片":"影片";
		$main_flag_panel = $("<div id='grabber-batch-flag'/>");
		$panel = $("<div id='airav-batch-title' style='float: left;'>"+batch_type_name+"<br/>批次中</div>").appendTo($main_flag_panel);
		//選擇
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		$panel.append("<span id='airav-check-all'>全選</span><span id='airav-check-cancel'>全不選</span>");
		$panel.append("<div>已選: <span id='airav-batch-checkeds'>0</span></div>");
		//標籤
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		tags = ($(selector_args["tag"]).length>0)?$(selector_args["tag"])[selector_args["tag_val"]]():"";
		$panel.append("<div style='line-height: 30px;margin-top: 15px;'>額外標籤: <input id='airav-batch-tags' value='"+tags+"' /></div>");

		//分類
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		$cates = $("<select id ='airav-batch-cates' />");
		// if(is_picture){
		// 	for(var i in albums_categorys){
		// 		$("<option/>").val(i).html(albums_categorys[i]).appendTo($cates);
		// 	}
		// }else{
		// 	for(var i in categorys){
		// 		$("<option/>").val(i).html(categorys[i]).appendTo($cates);
		// 	}
		// }
		$panel.append($("<div style='line-height: 30px;margin-top: 15px;'>分類: </div>").append($cates));

		//加入按鈕
		$panel = $("<div id='airav-batch-add' style='float: right;'>加入</div>").appendTo($main_flag_panel);
		$panel = $("<div class='airav-batch-disable' style='float: right;'>開啟</div>").appendTo($main_flag_panel);


		$main_flag_panel.appendTo("body");
		$("body").css("margin-top", 60);
		console.log("高度加好了~~~ grabber_init end");



    }





    // 取得主機位置 https://url.xxx.com
    function basename(path) {
		return path.replace(/\\/g,'/').replace( /.*\//, '' );
	}

	function dirname(path) {
		return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');;
	}

})();