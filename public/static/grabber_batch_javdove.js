
(function(){
	if(document.getElementById("airav-batch-flag")!=null){
		alert("已在批次中");
		return;
	}
	var is_picture = false;
	var categorys = <{$categorys}>;
	var albums_categorys = <{$albums_categorys}>;
	var script = document.getElementById("airav-script");
	var script_url = script.src;
	var css_url  = dirname(script_url) + '/grabber_batch.css';
	var func_enable = true ; // 是否啟用功能 add by Angus 2023.05.08

	a=document.createElement("link"),a.rel='stylesheet',a.type='text/css',a.href=css_url,a.media='all',document.body.appendChild(a);
	(function(){
		(a=document.createElement("script")).src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.js";
		document.body.appendChild(a);
		a.onload = function(){
			(a2=document.createElement("script")).src="<{$base_admin_url}>/modules/grabber/resource/jquery-ias.min.js";
			document.body.appendChild(a2);
			$.extend( $.expr[ ":" ], {
				data: $.expr.createPseudo ?
					$.expr.createPseudo(function( dataName ) {
						return function( elem ) {
							return !!$.data( elem, dataName );
						};
					}) :
					// support: jQuery <1.8
					function( elem, i, match ) {
						return !!$.data( elem, match[ 3 ] );
					}
			});
			var selector_args = {};
			switch(true){
				case location.href.indexOf('wokao.co')>-1:
				case location.href.indexOf('whichav.com')>-1:
					$("#ads1,#ads2").remove();
					selector_args = {
						"tag":"#keywordForm input[type=text]",
						"tag_val":"val",
						"main_a":".media-image>a",
						"click_a":".media-image>a,.block-title>a",
						"click_parent":"div.block"
							};
					break;
				case location.href.indexOf('pornhub.com')>-1:
					if(location.href.indexOf('pornhub.com/albums')>-1){
						is_picture = true;
						selector_args = {
								"tag":"#searchInput",
								"tag_val":"val",
								"main_a":".photoAlbumListBlock>a",
								"click_a":".photoAlbumListBlock>a",
								"click_parent":null
									};

					}else{
						selector_args = {
								"tag":"#categoriesStraight>ul>li>span.sidebarIndent",
								"tag_val":"text",
								"main_a":"div.img>a.img",
								"click_a":"div.img>a.img,span.title>a",
								"click_parent":"div.phimage"
									};

					}
					break;
				case location.href.indexOf('69vj.com')>-1:
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":"div.thumb-view>a.thumb-video",
						"click_a":"div.thumb-view>a.thumb-video",
						"click_parent":"div.video"
							};
					break;
				case location.href.indexOf('85porn.com')>-1:
					a2.onload = function(){
						var ias = $.ias({
							container : ".row .row",
							item : ">div.col-sm-6",
							pagination : ".pagination:eq(1)",
							next : ".prevnext"
						});
						ias.extension(new IASSpinnerExtension());
						ias.on('rendered', function(items) {
							var $items = $(items);

							$items.each(function() {
								if($(this).find("img").data("original"))
									$(this).find("img").attr("src",$(this).find("img").data("original"));
							});
						});
					};
					if(location.href.indexOf('85porn.com/albums')>-1){
						is_picture = true;
						selector_args = {
								"tag":"",
								"tag_val":"text",
								"main_a":"a:has(div.thumb-overlay)",
								"click_a":"a:has(div.thumb-overlay)",
								"click_parent":"div.well"
									};

					}else{
						selector_args = {
								"tag":"",
								"tag_val":"text",
								"main_a":"a:has(div.thumb-overlay)",
								"click_a":"a:has(div.thumb-overlay)",
								"click_parent":"div.well"
									};

					}
					break;
				case location.href.indexOf('91porn.com')>-1://有防擷取機制CF
					$(".imagechannel,.imagechannelhd").css({"position":"relative"});
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":".imagechannel>a,.imagechannelhd>a",
						"click_a":".imagechannel>a,.imagechannelhd>a,.listchannel>a",
						"click_parent":".listchannel"
							};
					break;
				case location.href.indexOf('xvideos.com')>-1:
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":"div.thumb>a",
						"click_a":"div.thumb>a,div.thumb-block>p>a,div.thumbInside>p>a",
						"click_parent":"div.thumb-block,div.thumbInside"
							};
					break;
				case location.href.indexOf('javdoe.com')>-1:
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":"a.main-thumb",
						"click_a":"div.wrap-main-item>a,div.wrap-item-meta>h3>a",
						"click_parent":"div.wrap-main-item"
							};
					break;
				case location.href.indexOf('youjizz.com')>-1:
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":".frame-wrapper>a:eq(0)",
						"click_a":".frame-wrapper>a,.video-title>a",
						"click_parent":"div.video-item"
							};
					break;
				case location.href.indexOf('zzcartoon.com')>-1:
					if(location.href.indexOf('zzcartoon.com/albums/')>-1){
						is_picture = true;
						$(".list_albums a").css({"position":"relative"});
						selector_args = {
							"tag":"",
							"tag_val":"text",
							"main_a":".list_albums a",
							"click_a":".list_albums a",
							"click_parent":null
								};
					}else{
						selector_args = {
							"tag":"",
							"tag_val":"text",
							"main_a":".list_videos a",
							"click_a":".list_videos a",
							"click_parent":null
								};
					}
					break;
				case location.href.indexOf('v.jav101.com')>-1:
					if(location.pathname.indexOf("/search/")==0){
						$("input[name=search]").val(decodeURI(location.pathname.substr("/search/".length)));
					}
					a2.onload = function(){
						var ias = $.ias({
							container : ".video-wrapper",
							item : ">div:has(.videoBox)",
							pagination : "#page",
							next : ".page_next"
						});
						ias.extension(new IASSpinnerExtension());
						ias.on('load', function(event) {
							// if(event.url==ias.nextUrl){
							// 	ias.getLastItem().next().remove();
							//     ias.destroy();
							// 	return false;
							// }
						});
						ias.on('rendered', function(items) {
							var $items = $(items);
							$items.each(function() {
								$(this).addClass("loaded");
								if($(this).find("img").data("original"))
									$(this).find("img").attr("src",$(this).find("img").data("original"));
							});
						});
					};
					selector_args = {
						"tag":"input[name=search]",
						"tag_val":"val",
						"main_a":"a.videoBox",
						"click_a":"a.videoBox",
						"click_parent":null
							};
					break;
				case location.href.indexOf('beeg.com')>-1:
					beeg=null;
					selector_args = {
						"tag":"form[name=search_form]>input[name=q]",
						"tag_val":"val",
						"main_a":".thumb-unit>a",
						"click_a":".thumb-unit>a",
						"click_parent":null
							};
					var tag = $(".thumblock-header").text().replace('Tag:','').trim();
					$("form[name=search_form]>input[name=q]").val(tag);
					break;
				case location.href.indexOf('avhd101.com')>-1:
					beeg=null;
					selector_args = {
						"tag":".search-txt",
						"tag_val":"val",
						"main_a":".list>li>a",
						"click_a":".list>li>a",
						"click_parent":null
							};
					break;
					case location.href.indexOf('iqqtv.net')>-1:
						selector_args = {
							"tag":"",
							"tag_val":"text",
							"main_a":".one-info-panel>a",
							"click_a":".one-info-panel>a",
							"click_parent":null
						};
						break;
				/*|--------------------------------------------------------------------------
				|*| https://hanime1.me/
				|*|--------------------------------------------------------------------------*/
				case location.href.indexOf('hanime1.me')>-1:
					selector_args = {
						"tag":"",
						"tag_val":"text",
						"main_a":".home-rows-videos-wrapper>a div, .home-doujin-videos.hidden-xs>a.overlay, .related-doujin-videos.hidden-xs>a.overlay, .search-doujin-videos.hidden-xs a.overlay",
						"click_a":".home-rows-videos-wrapper>a div, .home-doujin-videos.hidden-xs>a.overlay, .related-doujin-videos.hidden-xs>a.overlay, .search-doujin-videos.hidden-xs a.overlay",
						// "main_a":".home-doujin-videos.hidden-xs>a",
						// "click_a":".home-doujin-videos.hidden-xs>a",
						"click_parent":null
					};
					break;
				/* 說明區
				case location.href.indexOf('hanime1.me')>-1:
					selector_args = {
						"tag":".home-rows-videos-wrapper>a",
						"tag_val":"text",
						"main_a":"全選時jquery selector選擇器指向位置", // .home-rows-videos-wrapper>a div, .home-rows-videos-wrapper div.multiple-link-wrapper , .row div a
						"click_a":"需點選的影片位置",
						"click_parent":null
					};
					break;
				*/
				default:
					alert("無對應的模組");
					return;
			}
			airav_init(selector_args);
		};
	})();
	function basename(path) {
		return path.replace(/\\/g,'/').replace( /.*\//, '' );
	}

	function dirname(path) {
		return path.replace(/\\/g,'/').replace(/\/[^\/]*$/, '');;
	}

	function airav_init(selector_args){
		var batch_type_name = is_picture?"圖片":"影片";
		$main_flag_panel = $("<div id='airav-batch-flag'/>");

		$panel = $("<div id='airav-batch-title' style='float: left;'>"+batch_type_name+"<br/>批次中</div>").appendTo($main_flag_panel);
		//選擇
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		$panel.append("<span id='airav-check-all'>全選</span><span id='airav-check-cancel'>全不選</span>");
		$panel.append("<div>已選: <span id='airav-batch-checkeds'>0</span></div>");
		//標籤
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		tags = ($(selector_args["tag"]).length>0)?$(selector_args["tag"])[selector_args["tag_val"]]():"";
		$panel.append("<div style='line-height: 30px;margin-top: 15px;'>額外標籤: <input id='airav-batch-tags' value='"+tags+"' /></div>");

		//分類
		$panel = $("<div style='float: left;'/>").appendTo($main_flag_panel);
		$cates = $("<select id ='airav-batch-cates' />");
		if(is_picture){
			for(var i in albums_categorys){
				$("<option/>").val(i).html(albums_categorys[i]).appendTo($cates);
			}
		}else{
			for(var i in categorys){
				$("<option/>").val(i).html(categorys[i]).appendTo($cates);
			}
		}
		$panel.append($("<div style='line-height: 30px;margin-top: 15px;'>分類: </div>").append($cates));

		//加入按鈕
		$panel = $("<div id='airav-batch-add' style='float: right;'>加入</div>").appendTo($main_flag_panel);
		$panel = $("<div class='airav-batch-disable' style='float: right;'>開啟</div>").appendTo($main_flag_panel);
		$main_flag_panel.appendTo("body");
		$checked_flag = $("<div class='airav-batch-checked'>加入</div>"); // 已選擇的標籤
		$("body").css("margin-top", 60);

		/*|--------------------------------------------------------------------------
		|*| 綁定 全選按鈕
		|*|--------------------------------------------------------------------------*/
		$("#airav-check-all").click(function(){
			$(selector_args["main_a"]).filter(":not(:data(airavcheck))").data('airavcheck',1).append($checked_flag.clone());
			$("#airav-batch-checkeds").html($(selector_args["main_a"]).filter(":data(airavcheck)").length);
		});

		/*|--------------------------------------------------------------------------
		|*| 綁定 全不選按鈕
		|*|--------------------------------------------------------------------------*/
		$("#airav-check-cancel").click(function(){
			$(selector_args["main_a"]).filter(":data(airavcheck)").removeData('airavcheck').find('.airav-batch-checked').remove();
			$("#airav-batch-checkeds").html($(selector_args["main_a"]).filter(":data(airavcheck)").length);
		});


		/*|--------------------------------------------------------------------------
		|*| 綁定 需要選擇的影片位置
		|*|--------------------------------------------------------------------------*/
		$(document).on("click",selector_args["click_a"], function(e){
			if ( func_enable) return;	// 如果func_enable為true，則不執行
			e.preventDefault(); 		// 功用是阻止事件的預設行為發生

			/*|--------------------------------------------------------------------------
			|*| hanime1.me 結構不同 需另外處理
			|*|--------------------------------------------------------------------------*/
			if ( location.href.indexOf('hanime1.me')>-1 ) {
				$mediaimage_a = selector_args["click_parent"] ? $(this).closest(selector_args["click_parent"]).find(selector_args["main_a"]) : $(this);
				console.log( [ selector_args["click_parent"], $(this).closest(selector_args["click_parent"]).find(selector_args["main_a"]), $(this)] );
				if ( $(this).parent().attr('href')) {
					console.log( [$(this).parent().attr('href')]);
					if( $mediaimage_a.parent().data('airavcheck')){
						$mediaimage_a.parent().removeData('airavcheck') ;
						$mediaimage_a.find('.airav-batch-checked').remove();
					}else{
						$mediaimage_a.parent().data('airavcheck',1) ;
						$mediaimage_a.append($checked_flag.clone());
					}
				} else {
					console.log( [$(this).attr('href')]);
					if( $mediaimage_a.data('airavcheck')){
						$mediaimage_a.removeData('airavcheck') ;
						$mediaimage_a.find('.airav-batch-checked').remove();
					}else{
						$mediaimage_a.data('airavcheck',1) ;
						$mediaimage_a.append($checked_flag.clone());
					}
				}
				$("#airav-batch-checkeds").html($('a:data(airavcheck)').length);
			} else {
				/*|--------------------------------------------------------------------------
				|*| 這裡把已點選的換成
				|*|--------------------------------------------------------------------------*/
				if(!$(this).data("href")){
					$(this).data("href", $(this).prop("href")).prop("href", "javascript:void(0)");
				}
				$mediaimage_a = selector_args["click_parent"] ? $(this).closest(selector_args["click_parent"]).find(selector_args["main_a"]) : $(this);
				if( $mediaimage_a.data('airavcheck')){
					console.log($mediaimage_a.data('airavcheck'));
					$mediaimage_a.removeData('airavcheck').find('.airav-batch-checked').remove();
				}else{
					$mediaimage_a.data('airavcheck',1).append($checked_flag.clone());
				}
				$("#airav-batch-checkeds").html($("a:data(airavcheck)").length);
			}
		});

		$("#airav-batch-add").click(function(){
			var $select_items = $("a:data(airavcheck)");
			var tags = $("#airav-batch-tags").val();
			var cid = $("#airav-batch-cates").val();

			if($select_items.length==0){
				alert("未選取任何項目");
				return;
			}else if(!confirm("確認截取選取的["+$select_items.length+"]個項目?")){
				return;
			}

			var run_max = 5;
			var run_num = 0;
			var url_index = 0;
			var addAirav = function($titem){

				var url = $titem.data("href") ? $titem.data("href") : $titem.prop("href");
				if ( location.href.indexOf('hanime1.me')>-1 ) {
					url = $titem.attr("href");
				}
				var purl = "<{$base_admin_url}>/videos.php?m=add";
				// var purl = "<{$base_url}>/grabber_api.php?m=add";
				if(is_picture){
					purl = "<{$base_admin_url}>/albums.php?m=grabadd";
				}
				$.ajax({
					method:"POST",
					url:purl,
					dataType: "jsonp",
					data:{tags:tags, cid: cid, quick_url: url, jsonp: 1, garbber: 'javdove'}
				}).done(function(data){
					console.log(arguments);
					if(data.url_results){
						for(var i in data.url_results){
							var url_result = data.url_results[i];
							var $tmask = $titem.find(".airav-batch-checked");
							$tmask.html(url_result.msg);
							switch(url_result.code){
								case 0://成功
									$tmask.css({'background': '#84ef62'});
									break;
								case 1://加失敗
									$tmask.css({'background': '#d6ff00', 'color': '#000'});
									break;
								case 2://解析失敗
									$tmask.css({'background': '#0012ff'});
									break;
								case 3://已加入過
									$tmask.css({'background': '#9f00ff'});
									break;
								default:
									$tmask.css({'background': '#00abff'});

							}
						}
					}
					run_num--;
					while( (run_num < run_max) && (url_index < $select_items.length)){
						run_num++;
						addAirav($select_items.eq(url_index++));
					}
				});
			};
			while(run_num < run_max && url_index < $select_items.length){
				run_num++;
				addAirav( $select_items.eq(url_index++));
			}
		});

		/*|--------------------------------------------------------------------------
		|*| 綁定 關閉按鈕
		|*|--------------------------------------------------------------------------*/
		$(document).on("click",'.airav-batch-disable', function(e){
			$(this).addClass("airav-batch-enable").removeClass("airav-batch-disable").html("關閉");
			func_enable = false;
		});

		$(document).on("click",'.airav-batch-enable', function(e){
			$(this).addClass("airav-batch-disable").removeClass("airav-batch-enable").html("開啟");
			func_enable = true;
		});
	}
})();