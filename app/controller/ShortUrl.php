<?php

declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use think\Request;
use think\facade\View ;

// use app\model\ShortUrl as ShortUrlModel ;
use app\model\ShortUrlModel ;


class ShortUrl extends BaseController
{
	/**
	 * [redirect 短網址轉址]
	 *
	 * @param [type] $shorturl
	 * @return void
	 */
	public function redirect( $shorturl) {
		$objShortUrl = new ShortUrlModel ;
		$filetrs = [] ;
		$filetrs = [ 'short_url' => $shorturl,];
		$result = $objShortUrl->findUrl( $filetrs) ;
		if ( $result) {
			$objShortUrl->hitLog( $result) ;
			return redirect( $result['origin_url'], 301 ) ;
		} else {
			return redirect( '/404.html' ) ;
		}
	}

	/**
	 * [clear 清除短網址資料庫資料]
	 *
	 * @param [type] $shorturl
	 * @return void
	 */
	public function clear( $shorturl) {
		$objShortUrl = new ShortUrlModel ;
		$result = $objShortUrl->findUrl( $shorturl ) ;
		// TODO 清除短網址資料庫資料
	}

	public function createPage() {
		// dump( [ "php think make:controller ShortUrl"] ) ;
		// echo ShortUrlModel::random( 6 ) ;
		// dump( [ __METHOD__] ) ;
		// return '您好！ 這是一個[ShortUrl]範例應用';

		return View::fetch() ;
	}

	public function createUrl() {
		// dump( __METHOD__) ;
		request()->isAjax() or die( '非法操作' ) ;
		/*|------------------------------------------------------------------------------
		|*| 回傳格式
		|*|----------------------------------------------------------------------------*/
		$retJson = [
			'code' => 200,
			'msg' => 'success',
			'short_url' => '',
		] ;

		$params = request()->param() ; // 取得POST參數
		// TODO 產生短網址 Generation of short URLs
		$objShortUrl = new ShortUrlModel ;
		$filters = [] ;
		$filters = [ 'origin_url' => $params['original_url'],];
		// 檢查是否已經有短網址
		$result = $objShortUrl->findUrl( $filters) ;
		if ( $result) {
			// 已經有短網址
			$shorturl = $result['short_url'] ;
			$retJson['msg'] = '已經有短網址' ;
			unset( $result) ;
		} else {
			$filetrs = [] ;
			// 產生新的短網址
			do {
				$shorturl = ShortUrlModel::random( 6 ) ;
				$filetrs = [ 'short_url' => $shorturl,];
				$result = $objShortUrl->findUrl( $filetrs) ;
			} while ( $result) ;
			// TODO 儲存短網址
			$objShortUrl->insShortUrl( $shorturl, $params['original_url']) ;
			$retJson['msg'] = '產生新的短網址' ;
		}
		// TODO 回傳短網址
		$retShortUrl = request()->domain() . '/' . $shorturl ;
		$retJson['short_url'] = $retShortUrl ;
		return json( $retJson ) ;
	}


	/**
	 * 显示资源列表
	 *
	 * @return \think\Response
	 */
	public function index( $shorturl)
	{
		dump( [ "php think make:controller ShortUrl"] ) ;
		dump( [ __METHOD__, $shorturl] ) ;
		return '您好！ 這是一個[ShortUrl]範例應用';
	}

	/**
	 * 显示创建资源表单页.
	 *
	 * @return \think\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * 保存新建的资源
	 *
	 * @param  \think\Request  $request
	 * @return \think\Response
	 */
	public function save(Request $request)
	{
		//
	}

	/**
	 * 显示指定的资源
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function read($id)
	{
		//
	}

	/**
	 * 显示编辑资源表单页.
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * 保存更新的资源
	 *
	 * @param  \think\Request  $request
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * 删除指定资源
	 *
	 * @param  int  $id
	 * @return \think\Response
	 */
	public function delete($id)
	{
		//
	}
}
