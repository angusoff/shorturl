<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;
use think\facade\Db;
// use think\db\Query;

/**
 * @mixin \think\Model
 */
class ShortUrlModel extends Model
{
	protected $table = 'short_urls';

	/**
	 * [findUrl 查詢短網址]
	 *
	 * @param [type] $shorturl
	 * @return void
	 */
	public function findUrl( $filters) {
		// $filters = [
		// 	'short_url'	=> $shorturl,
		// 	'origin_url' => $original_url,
		// ];

		$result = Db::table('short_urls')->where( $filters)->find();
		return $result;
	}

	/**
	 * [hitLog 短網址點擊紀錄]
	 *
	 * @param [type] $rowData
	 * @return void
	 */
	public function hitLog( $rowData) {

		/*|------------------------------------------------------------------------------
		|*| 短網址點擊紀錄
		|*|----------------------------------------------------------------------------*/
		Db::table('short_urls')->where('short_url', $rowData['short_url'])
			->inc('clicks')->update() ;
		/*|------------------------------------------------------------------------------
		|*| 短網址點擊紀錄
		|*|----------------------------------------------------------------------------*/
		$ip = $this->get_real_ip() ;

		$insData = [
			'short_url'	=> $rowData['short_url'],
			'ip'		=> $ip,
			'created_at'=> date('Y-m-d H:i:s'),
		];
		Db::table('short_url_logs')->insert( $insData) ;
	}

	/**
	 * [insShortUrl 新增短網址]
	 *
	 * @return void
	 */
	public function insShortUrl( $shorturl, $original_url) {
		/*|------------------------------------------------------------------------------
		|*| 短網址點擊紀錄
		|*|----------------------------------------------------------------------------*/
		Db::table('short_urls')->insert([
			'user_id'		=> $shorturl,
			'short_url'		=> $shorturl,
			'origin_url'	=> $original_url,
			'host'			=> $_SERVER['HTTP_HOST'],
			'clicks'		=> 0,
			'created_at'	=> date('Y-m-d H:i:s'),
		]);
		/*|------------------------------------------------------------------------------
		|*| 短網址點擊紀錄
		|*|----------------------------------------------------------------------------*/

	}

	/**
	 * [random 短網址亂數]
	 *
	 * @param [type] $length
	 * @param string $pool
	 * @return void
	 */
	public static function random($length, $pool = '')
	{
		$random = '';
		if (empty($pool)) {
			$pool = 'abcdefghkmnpqrstuvwxyz';
			$pool .= 'ABCDEFGHJKLMNPQRSTUVWXYZ';
			$pool .= '23456789';
		}
		// srand(microtime() * 1000000);

		for ($i = 0; $i < $length; $i++) {
			$random .= substr($pool, (rand() % (strlen($pool))), 1);
		}
		return $random;
	}

	public static function get_real_ip()
    {
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $cip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif(!empty($_SERVER["REMOTE_ADDR"])){
            $cip = $_SERVER["REMOTE_ADDR"];
        }
        else{
            $cip = "無法取得IP";
        }
        return $cip;
    }
}
