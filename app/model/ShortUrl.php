<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class ShortUrl extends Model
{


	/**
	 * [random 短網址亂數]
	 *
	 * @param [type] $length
	 * @param string $pool
	 * @return void
	 */
	public static function random($length, $pool = '')
	{
		$random = '';
		if (empty($pool)) {
			$pool = 'abcdefghkmnpqrstuvwxyz';
			$pool .= 'ABCDEFGHJKLMNPQRSTUVWXYZ';
			$pool .= '23456789';
		}
		// srand(microtime() * 1000000);

		for ($i = 0; $i < $length; $i++) {
			$random .= substr($pool, (rand() % (strlen($pool))), 1);
		}
		return $random;
	}
}
