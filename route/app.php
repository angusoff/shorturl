<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// Route::get('think', function () {
//     return 'hello,ThinkPHP6!';
// });

// Route::get('hello/:name', 'index/hello');



// 執行區
Route::get('/create_su', 'ShortUrl/createPage') ; // 產生短網址頁面
Route::post('/create_url', 'ShortUrl/createUrl') ; // 產生短網址

Route::get('/:shorturl/clear', 'ShortUrl/clear') ; // 清除短網址快取
Route::get('/:shorturl', 'ShortUrl/redirect') ; // 短網址轉址

// 範例區
/*|----------------------------------------------------------------------------
|*| php think build ShortUrl 產生的類別檔案 路由設定
|*| 產生的類別檔案位於 app/ShortUrl/controller/index.php
|*| 會自動生成 controller model view的目錄
|*| 及common.php middleware.php event.php的檔案
|*| 路由的指定方式有兩種
|*|----------------------------------------------------------------------------*/
// Route::get('/:shorturl', '\app\ShortUrl\controller\index@index') ;
// Route::get('/:shorturl', '\app\ShortUrl\controller\index::index') ;

/*|----------------------------------------------------------------------------
|*| php think make:controller ShortUrl 產生的類別檔案 路由設定
|*| 產生的類別檔案位於 app/controller/ShortUrl.php
|*|----------------------------------------------------------------------------*/
// Route::get('/:shorturl', 'ShortUrl/index') ;

/*|----------------------------------------------------------------------------
|*| php think make:controller ShortUrl/ShortUrl 產生的類別檔案 路由設定
|*| 產生的類別檔案位於 app/controller/ShortUrl/ShortUrl.php
|*|----------------------------------------------------------------------------*/
// Route::get('/:shorturl', 'ShortUrl.ShortUrl/index') ;

/*|----------------------------------------------------------------------------
|*| php think make:controller ShortUrl@ShortUrl 產生的類別檔案 路由設定
|*| 產生的類別檔案位於 app/ShortUrl/controller/ShortUrl.php
|*|----------------------------------------------------------------------------*/
// Route::get('/:shorturl', '\app\ShortUrl\controller\ShortUrl@index') ;


// Route::miss( function(){
//     return redirect( '/404.html' ) ;
// });